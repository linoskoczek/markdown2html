
//----------------------------------------------------
// The following code was generated by CUP v0.11b 20141204 (SVN rev 60)
//----------------------------------------------------

package cup.example;

import java_cup.runtime.*;
import cup.example.Lexer;
import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;
import java.lang.Object;
import java.util.*;
import java_cup.runtime.ComplexSymbolFactory.Location;
import java_cup.runtime.XMLElement;

/** CUP v0.11b 20141204 (SVN rev 60) generated parser.
  */
@SuppressWarnings({"rawtypes"})
public class Parser extends java_cup.runtime.lr_parser {

 public final Class getSymbolContainer() {
    return sym.class;
}

  /** Default constructor. */
  public Parser() {super();}

  /** Constructor which sets the default scanner. */
  public Parser(java_cup.runtime.Scanner s) {super(s);}

  /** Constructor which sets the default scanner. */
  public Parser(java_cup.runtime.Scanner s, java_cup.runtime.SymbolFactory sf) {super(s,sf);}

  /** Production table. */
  protected static final short _production_table[][] = 
    unpackFromStrings(new String[] {
    "\000\022\000\002\002\004\000\002\002\004\000\002\002" +
    "\002\000\002\004\004\000\002\004\004\000\002\004\004" +
    "\000\002\004\003\000\002\004\003\000\002\004\003\000" +
    "\002\004\003\000\002\004\003\000\002\004\003\000\002" +
    "\004\010\000\002\004\003\000\002\004\003\000\002\004" +
    "\004\000\002\004\003\000\002\004\003" });

  /** Access to production table. */
  public short[][] production_table() {return _production_table;}

  /** Parse-action table. */
  protected static final short[][] _action_table = 
    unpackFromStrings(new String[] {
    "\000\034\000\042\002\uffff\004\uffff\005\uffff\006\uffff\010" +
    "\uffff\011\uffff\012\uffff\013\uffff\014\uffff\015\uffff\017\uffff" +
    "\020\uffff\021\uffff\022\uffff\023\uffff\024\uffff\001\002\000" +
    "\042\002\015\004\024\005\005\006\006\010\012\011\011" +
    "\012\014\013\016\014\022\015\017\017\010\020\021\021" +
    "\023\022\007\023\013\024\025\001\002\000\040\004\024" +
    "\005\005\006\006\010\012\011\011\012\014\013\016\014" +
    "\022\015\017\017\010\020\021\021\023\022\007\023\013" +
    "\024\025\001\002\000\040\004\024\005\005\006\006\010" +
    "\012\011\011\012\014\013\016\014\022\015\017\017\010" +
    "\020\021\021\023\022\007\023\013\024\025\001\002\000" +
    "\044\002\ufff7\004\ufff7\005\ufff7\006\ufff7\010\ufff7\011\ufff7" +
    "\012\ufff7\013\ufff7\014\ufff7\015\ufff7\016\ufff7\017\ufff7\020" +
    "\ufff7\021\ufff7\022\ufff7\023\ufff7\024\ufff7\001\002\000\044" +
    "\002\ufff6\004\ufff6\005\ufff6\006\ufff6\010\ufff6\011\ufff6\012" +
    "\ufff6\013\ufff6\014\ufff6\015\ufff6\016\ufff6\017\ufff6\020\ufff6" +
    "\021\ufff6\022\ufff6\023\ufff6\024\ufff6\001\002\000\044\002" +
    "\ufff9\004\ufff9\005\ufff9\006\ufff9\010\ufff9\011\ufff9\012\ufff9" +
    "\013\ufff9\014\ufff9\015\ufff9\016\ufff9\017\ufff9\020\ufff9\021" +
    "\ufff9\022\ufff9\023\ufff9\024\ufff9\001\002\000\040\004\024" +
    "\005\005\006\006\010\012\011\011\012\014\013\016\014" +
    "\022\015\017\017\010\020\021\021\023\022\007\023\013" +
    "\024\025\001\002\000\044\002\ufff0\004\ufff0\005\ufff0\006" +
    "\ufff0\010\ufff0\011\ufff0\012\ufff0\013\ufff0\014\ufff0\015\ufff0" +
    "\016\ufff0\017\ufff0\020\ufff0\021\ufff0\022\ufff0\023\ufff0\024" +
    "\ufff0\001\002\000\044\002\ufffb\004\ufffb\005\ufffb\006\ufffb" +
    "\010\ufffb\011\ufffb\012\ufffb\013\ufffb\014\ufffb\015\ufffb\016" +
    "\ufffb\017\ufffb\020\ufffb\021\ufffb\022\ufffb\023\ufffb\024\ufffb" +
    "\001\002\000\004\002\000\001\002\000\044\002\ufffa\004" +
    "\ufffa\005\ufffa\006\ufffa\010\ufffa\011\ufffa\012\ufffa\013\ufffa" +
    "\014\ufffa\015\ufffa\016\ufffa\017\ufffa\020\ufffa\021\ufffa\022" +
    "\ufffa\023\ufffa\024\ufffa\001\002\000\044\002\ufff3\004\ufff3" +
    "\005\ufff3\006\ufff3\010\ufff3\011\ufff3\012\ufff3\013\ufff3\014" +
    "\ufff3\015\ufff3\016\ufff3\017\ufff3\020\ufff3\021\ufff3\022\ufff3" +
    "\023\ufff3\024\ufff3\001\002\000\042\002\001\004\001\005" +
    "\001\006\001\010\001\011\001\012\001\013\001\014\001" +
    "\015\001\017\001\020\001\021\001\022\001\023\001\024" +
    "\001\001\002\000\040\004\024\005\005\006\006\010\012" +
    "\011\011\012\014\013\016\014\022\015\017\017\010\020" +
    "\021\021\023\022\007\023\013\024\025\001\002\000\044" +
    "\002\ufff4\004\ufff4\005\ufff4\006\ufff4\010\ufff4\011\ufff4\012" +
    "\ufff4\013\ufff4\014\ufff4\015\ufff4\016\ufff4\017\ufff4\020\ufff4" +
    "\021\ufff4\022\ufff4\023\ufff4\024\ufff4\001\002\000\044\002" +
    "\ufff8\004\ufff8\005\ufff8\006\ufff8\010\ufff8\011\ufff8\012\ufff8" +
    "\013\ufff8\014\ufff8\015\ufff8\016\ufff8\017\ufff8\020\ufff8\021" +
    "\ufff8\022\ufff8\023\ufff8\024\ufff8\001\002\000\040\004\024" +
    "\005\005\006\006\010\012\011\011\012\014\013\016\014" +
    "\022\015\017\017\010\020\021\021\023\022\007\023\013" +
    "\024\025\001\002\000\044\002\ufff1\004\ufff1\005\ufff1\006" +
    "\ufff1\010\ufff1\011\ufff1\012\ufff1\013\ufff1\014\ufff1\015\ufff1" +
    "\016\ufff1\017\ufff1\020\ufff1\021\ufff1\022\ufff1\023\ufff1\024" +
    "\ufff1\001\002\000\044\002\ufffc\004\ufffc\005\ufffc\006\ufffc" +
    "\010\ufffc\011\ufffc\012\ufffc\013\ufffc\014\ufffc\015\ufffc\016" +
    "\ufffc\017\ufffc\020\ufffc\021\ufffc\022\ufffc\023\ufffc\024\ufffc" +
    "\001\002\000\004\016\030\001\002\000\004\014\031\001" +
    "\002\000\004\025\032\001\002\000\004\015\033\001\002" +
    "\000\044\002\ufff5\004\ufff5\005\ufff5\006\ufff5\010\ufff5\011" +
    "\ufff5\012\ufff5\013\ufff5\014\ufff5\015\ufff5\016\ufff5\017\ufff5" +
    "\020\ufff5\021\ufff5\022\ufff5\023\ufff5\024\ufff5\001\002\000" +
    "\044\002\ufff2\004\ufff2\005\ufff2\006\ufff2\010\ufff2\011\ufff2" +
    "\012\ufff2\013\ufff2\014\ufff2\015\ufff2\016\ufff2\017\ufff2\020" +
    "\ufff2\021\ufff2\022\ufff2\023\ufff2\024\ufff2\001\002\000\044" +
    "\002\ufffe\004\ufffe\005\ufffe\006\ufffe\010\ufffe\011\ufffe\012" +
    "\ufffe\013\ufffe\014\ufffe\015\ufffe\016\ufffe\017\ufffe\020\ufffe" +
    "\021\ufffe\022\ufffe\023\ufffe\024\ufffe\001\002\000\044\002" +
    "\ufffd\004\ufffd\005\ufffd\006\ufffd\010\ufffd\011\ufffd\012\ufffd" +
    "\013\ufffd\014\ufffd\015\ufffd\016\ufffd\017\ufffd\020\ufffd\021" +
    "\ufffd\022\ufffd\023\ufffd\024\ufffd\001\002" });

  /** Access to parse-action table. */
  public short[][] action_table() {return _action_table;}

  /** <code>reduce_goto</code> table. */
  protected static final short[][] _reduce_table = 
    unpackFromStrings(new String[] {
    "\000\034\000\004\002\003\001\001\000\004\004\017\001" +
    "\001\000\004\004\035\001\001\000\004\004\034\001\001" +
    "\000\002\001\001\000\002\001\001\000\002\001\001\000" +
    "\004\004\033\001\001\000\002\001\001\000\002\001\001" +
    "\000\002\001\001\000\002\001\001\000\002\001\001\000" +
    "\002\001\001\000\004\004\026\001\001\000\002\001\001" +
    "\000\002\001\001\000\004\004\025\001\001\000\002\001" +
    "\001\000\002\001\001\000\002\001\001\000\002\001\001" +
    "\000\002\001\001\000\002\001\001\000\002\001\001\000" +
    "\002\001\001\000\002\001\001\000\002\001\001" });

  /** Access to <code>reduce_goto</code> table. */
  public short[][] reduce_table() {return _reduce_table;}

  /** Instance of action encapsulation class. */
  protected CUP$Parser$actions action_obj;

  /** Action encapsulation object initializer. */
  protected void init_actions()
    {
      action_obj = new CUP$Parser$actions(this);
    }

  /** Invoke a user supplied parse action. */
  public java_cup.runtime.Symbol do_action(
    int                        act_num,
    java_cup.runtime.lr_parser parser,
    java.util.Stack            stack,
    int                        top)
    throws java.lang.Exception
  {
    /* call code in generated class */
    return action_obj.CUP$Parser$do_action(act_num, parser, stack, top);
  }

  /** Indicates start state. */
  public int start_state() {return 0;}
  /** Indicates start production. */
  public int start_production() {return 1;}

  /** <code>EOF</code> Symbol index. */
  public int EOF_sym() {return 0;}

  /** <code>error</code> Symbol index. */
  public int error_sym() {return 1;}


  /** User initialization code. */
  public void user_init() throws java.lang.Exception
    {
//@@CUPDBG1

  ComplexSymbolFactory f = new ComplexSymbolFactory();
  symbolFactory = f;
  File file = new File("inputMulti.txt");
  FileInputStream fis = null;
  try {
    fis = new FileInputStream(file);
  } catch (IOException e) {
    e.printStackTrace();
  }
  lexer = new Lexer(f,fis);

    }

  /** Scan to get the next Symbol. */
  public java_cup.runtime.Symbol scan()
    throws java.lang.Exception
    {
//@@CUPDBG2
 return lexer.next_token(); 
    }

//@@CUPDBG0

  protected Lexer lexer;
  boolean bold, italic, h1, h2, h3, strike, list, code;


/** Cup generated class to encapsulate user supplied action code.*/
@SuppressWarnings({"rawtypes", "unchecked", "unused"})
class CUP$Parser$actions {
  private final Parser parser;

  /** Constructor */
  CUP$Parser$actions(Parser parser) {
    this.parser = parser;
  }

  /** Method 0 with the actual generated action code for actions 0 to 300. */
  public final java_cup.runtime.Symbol CUP$Parser$do_action_part00000000(
    int                        CUP$Parser$act_num,
    java_cup.runtime.lr_parser CUP$Parser$parser,
    java.util.Stack            CUP$Parser$stack,
    int                        CUP$Parser$top)
    throws java.lang.Exception
    {
      /* Symbol object for return from actions */
      java_cup.runtime.Symbol CUP$Parser$result;

      /* select the action based on the action number */
      switch (CUP$Parser$act_num)
        {
          /*. . . . . . . . . . . . . . . . . . . .*/
          case 0: // list ::= list expr 
            {
              Object RESULT =null;
		Location exleft = ((java_cup.runtime.ComplexSymbolFactory.ComplexSymbol)CUP$Parser$stack.peek()).xleft;
		Location exright = ((java_cup.runtime.ComplexSymbolFactory.ComplexSymbol)CUP$Parser$stack.peek()).xright;
		String e = (String)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		//@@CUPDBG3
 System.out.print(e != null ? e : ""); 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("list",0, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 1: // $START ::= list EOF 
            {
              Object RESULT =null;
		Location start_valxleft = ((java_cup.runtime.ComplexSymbolFactory.ComplexSymbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)).xleft;
		Location start_valxright = ((java_cup.runtime.ComplexSymbolFactory.ComplexSymbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)).xright;
		Object start_val = (Object)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-1)).value;
		RESULT = start_val;
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("$START",0, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          /* ACCEPT */
          CUP$Parser$parser.done_parsing();
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 2: // list ::= 
            {
              Object RESULT =null;

              CUP$Parser$result = parser.getSymbolFactory().newSymbol("list",0, ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 3: // expr ::= HEADER1 expr 
            {
              String RESULT =null;
		Location exleft = ((java_cup.runtime.ComplexSymbolFactory.ComplexSymbol)CUP$Parser$stack.peek()).xleft;
		Location exright = ((java_cup.runtime.ComplexSymbolFactory.ComplexSymbol)CUP$Parser$stack.peek()).xright;
		String e = (String)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		//@@CUPDBG4

                    String res = "<h1>" + e;
                    if(RESULT == null) RESULT = res;
                    else RESULT += res;
                    h1 = !h1;
                    
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("expr",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 4: // expr ::= HEADER2 expr 
            {
              String RESULT =null;
		Location exleft = ((java_cup.runtime.ComplexSymbolFactory.ComplexSymbol)CUP$Parser$stack.peek()).xleft;
		Location exright = ((java_cup.runtime.ComplexSymbolFactory.ComplexSymbol)CUP$Parser$stack.peek()).xright;
		String e = (String)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		//@@CUPDBG5

                     String res = "<h2>" + e;
                     if(RESULT == null) RESULT = res;
                     else RESULT += res;
                     h2 = !h2;
                     
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("expr",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 5: // expr ::= HEADER3 expr 
            {
              String RESULT =null;
		Location exleft = ((java_cup.runtime.ComplexSymbolFactory.ComplexSymbol)CUP$Parser$stack.peek()).xleft;
		Location exright = ((java_cup.runtime.ComplexSymbolFactory.ComplexSymbol)CUP$Parser$stack.peek()).xright;
		String e = (String)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		//@@CUPDBG6

                     String res = "<h3>" + e;
                     if(RESULT == null) RESULT = res;
                     else RESULT += res;
                     h3 = !h3;
                     
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("expr",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 6: // expr ::= STAR 
            {
              String RESULT =null;
		//@@CUPDBG7

                    String res = "<i>";
                    if(italic) res = "</i>";
                    if(RESULT == null) RESULT = res;
                    else RESULT += res;
                    italic = !italic;
                   
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("expr",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 7: // expr ::= DSTAR 
            {
              String RESULT =null;
		//@@CUPDBG8

                    String res = "<b>";
                    if(bold) res = "</b>";
                    if(RESULT == null) RESULT = res;
                    else RESULT += res;
                    bold = !bold;
                   
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("expr",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 8: // expr ::= CIAP 
            {
              String RESULT =null;
		//@@CUPDBG9

                    String res = "<code>";
                    if(code) res = "</code>";
                    if(RESULT == null) RESULT = res;
                    else RESULT += res;
                    code = !code;
                   
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("expr",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 9: // expr ::= UNDERSCORE 
            {
              String RESULT =null;
		//@@CUPDBG10

                    String res = "<i>";
                    if(italic) res = "</i>";
                    if(RESULT == null) RESULT = res;
                    else RESULT += res;
                    italic = !italic;
                   
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("expr",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 10: // expr ::= DUNDERSCORE 
            {
              String RESULT =null;
		//@@CUPDBG11

                    String res = "<b>";
                    if(bold) res = "</b>";
                    if(RESULT == null) RESULT = res;
                    else RESULT += res;
                    bold = !bold;
                   
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("expr",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 11: // expr ::= ESC 
            {
              String RESULT =null;
		Location exleft = ((java_cup.runtime.ComplexSymbolFactory.ComplexSymbol)CUP$Parser$stack.peek()).xleft;
		Location exright = ((java_cup.runtime.ComplexSymbolFactory.ComplexSymbol)CUP$Parser$stack.peek()).xright;
		Object e = (Object)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		//@@CUPDBG12
 if(RESULT == null) RESULT = ""+e; else RESULT += ""+e; 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("expr",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 12: // expr ::= LSQPAREN expr RSQPAREN LPAREN URL RPAREN 
            {
              String RESULT =null;
		Location exleft = ((java_cup.runtime.ComplexSymbolFactory.ComplexSymbol)CUP$Parser$stack.elementAt(CUP$Parser$top-4)).xleft;
		Location exright = ((java_cup.runtime.ComplexSymbolFactory.ComplexSymbol)CUP$Parser$stack.elementAt(CUP$Parser$top-4)).xright;
		String e = (String)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-4)).value;
		Location uxleft = ((java_cup.runtime.ComplexSymbolFactory.ComplexSymbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)).xleft;
		Location uxright = ((java_cup.runtime.ComplexSymbolFactory.ComplexSymbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)).xright;
		String u = (String)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-1)).value;
		//@@CUPDBG13

                    String res = "<a href=\""+u+"\">"+e+"</a>";
                    if(RESULT == null) RESULT = res; else RESULT += res;
                    
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("expr",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-5)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 13: // expr ::= LPAREN 
            {
              String RESULT =null;
		//@@CUPDBG14
 if(RESULT == null) RESULT = "("; else RESULT += "("; 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("expr",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 14: // expr ::= RPAREN 
            {
              String RESULT =null;
		//@@CUPDBG15
 if(RESULT == null) RESULT = ")"; else RESULT += ")"; 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("expr",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 15: // expr ::= LI expr 
            {
              String RESULT =null;
		Location exleft = ((java_cup.runtime.ComplexSymbolFactory.ComplexSymbol)CUP$Parser$stack.peek()).xleft;
		Location exright = ((java_cup.runtime.ComplexSymbolFactory.ComplexSymbol)CUP$Parser$stack.peek()).xright;
		String e = (String)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		//@@CUPDBG16
  String res = "";
                        if(!list) { res += "<ul>"; list = !list; }
                        if(RESULT == null) RESULT = res + "<li>"+e;
                        else RESULT += res + "<li>"+e;
                    
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("expr",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 16: // expr ::= WORD 
            {
              String RESULT =null;
		Location wxleft = ((java_cup.runtime.ComplexSymbolFactory.ComplexSymbol)CUP$Parser$stack.peek()).xleft;
		Location wxright = ((java_cup.runtime.ComplexSymbolFactory.ComplexSymbol)CUP$Parser$stack.peek()).xright;
		String w = (String)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		//@@CUPDBG17
 if(RESULT == null) RESULT = w+""; else RESULT += w+""; 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("expr",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 17: // expr ::= SEMI 
            {
              String RESULT =null;
		//@@CUPDBG18
   bold = false; italic = false;
                    String h = "";
                    if(h1) h = "</h1>"; else if(h2) h = "</h2>"; else if (h3) h = "</h3>";
                    h1 = h2 = h3 = false;
                    if(RESULT == null)
                        RESULT = h;
                    if(!list) {RESULT += "\n"; }
                    else { RESULT += "</li></ul>\n"; list = !list; }
                
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("expr",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /* . . . . . .*/
          default:
            throw new Exception(
               "Invalid action number "+CUP$Parser$act_num+"found in internal parse table");

        }
    } /* end of method */

  /** Method splitting the generated action code into several parts. */
  public final java_cup.runtime.Symbol CUP$Parser$do_action(
    int                        CUP$Parser$act_num,
    java_cup.runtime.lr_parser CUP$Parser$parser,
    java.util.Stack            CUP$Parser$stack,
    int                        CUP$Parser$top)
    throws java.lang.Exception
    {
              return CUP$Parser$do_action_part00000000(
                               CUP$Parser$act_num,
                               CUP$Parser$parser,
                               CUP$Parser$stack,
                               CUP$Parser$top);
    }
}

}
