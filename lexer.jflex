package cup.example;
import java_cup.runtime.ComplexSymbolFactory;
import java_cup.runtime.ComplexSymbolFactory.Location;
import java_cup.runtime.Symbol;
import java.lang.*;

%%

%class Lexer
%implements sym
%public
%unicode
%line
%column
%cup
%char
%{
	

    public Lexer(ComplexSymbolFactory sf, java.io.InputStream is){
		this(is);
        symbolFactory = sf;
    }
	public Lexer(ComplexSymbolFactory sf, java.io.Reader reader){
		this(reader);
        symbolFactory = sf;
    }

    private StringBuffer sb = new StringBuffer();
    private ComplexSymbolFactory symbolFactory;
    private int csline,cscolumn;

    public Symbol symbol(String name, int code){
		return symbolFactory.newSymbol(name, code,
						new Location(yyline+1,yycolumn+1, yychar), // -yylength()
						new Location(yyline+1,yycolumn+yylength(), yychar+yylength())
				);
    }
    public Symbol symbol(String name, int code, String lexem){
	return symbolFactory.newSymbol(name, code,
						new Location(yyline+1, yycolumn +1, yychar),
						new Location(yyline+1,yycolumn+yylength(), yychar+yylength()), lexem);
    }

    protected void emit_warning(String message){
    	System.out.println("scanner warning: " + message + " at : 2 "+
    			(yyline+1) + " " + (yycolumn+1) + " " + yychar);
    }

    protected void emit_error(String message){
    	System.out.println("scanner error: " + message + " at : 2" +
    			(yyline+1) + " " + (yycolumn+1) + " " + yychar);
    }
%}

New        = \r|\n|\r\n
DblNew     = (\n\n)|(\r\n\r\n)
Whitespace = [ \t\f]
Url        = (https?:\/\/[a-zA-Z0-9\-\.\/\?\&%_]+)
Word       = [^_*#`\r\n\[\]\(\)]+
Esc        = (\#)|(\\\[)
DblTilde   = \-\-

%eofval{
    return symbolFactory.newSymbol("EOF",sym.EOF);
%eofval}

%state STRING
%state LIST

%%  

<YYINITIAL> {
    {Whitespace} {  }
    "```"        { sb.setLength(0); sb.append("<code>"); yybegin(STRING); }
    "# "         { return symbolFactory.newSymbol("HEADER1", HEADER1); }
    "## "        { return symbolFactory.newSymbol("HEADER2", HEADER2); }
    "### "       { return symbolFactory.newSymbol("HEADER3", HEADER3); }
    "* "         { sb.setLength(0); sb.append("<ul>\n<li>"); return symbolFactory.newSymbol("LI", LI, sb.toString()); }
    "*"          { return symbolFactory.newSymbol("STAR", STAR); }
    "**"         { return symbolFactory.newSymbol("DSTAR", DSTAR); }
    "_"          { return symbolFactory.newSymbol("UNDERSCORE", UNDERSCORE); }
    "__"         { return symbolFactory.newSymbol("DUNDERSCORE", DUNDERSCORE); }
    "`"          { return symbolFactory.newSymbol("CIAP", CIAP); }
    "("          { return symbolFactory.newSymbol("LPAREN", LPAREN); }
    ")"          { return symbolFactory.newSymbol("RPAREN", RPAREN); }
    "["		     { return symbolFactory.newSymbol("LSQPAREN", LSQPAREN); }
    "]"		     { return symbolFactory.newSymbol("RSQPAREN", RSQPAREN); }
    {Esc}        { return symbolFactory.newSymbol("ESC", ESC, yytext()); }
    {New}		 { return symbolFactory.newSymbol("SEMI", SEMI); }
    {Url}        { return symbolFactory.newSymbol("URL", URL, yytext()); }
    {Word}       { return symbolFactory.newSymbol("WORD", WORD, yytext()); }
}

<STRING> {
    "```"        { yybegin(YYINITIAL); return symbolFactory.newSymbol("WORD", WORD, sb.toString() + "</code>"); }
    {New}        { sb.append("<br>\n"); }
    .            { sb.append(yytext()); }
}

// error fallback
//.             { System.out.println(yytext()); }
.|\n          { emit_warning("Unrecognized character '" +yytext()+"' -- ignored"); }
